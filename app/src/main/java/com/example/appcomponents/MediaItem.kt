package com.example.appcomponents

import android.net.Uri

data class MediaItem(
    val itemId: Long,
    val uri: Uri,
    val path: String
)