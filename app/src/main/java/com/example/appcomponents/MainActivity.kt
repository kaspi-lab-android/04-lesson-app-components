package com.example.appcomponents

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

private const val REQUEST_CODE_SECOND_ACTIVITY = 1000

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivityButton.setOnClickListener {
            val text = sendText.text.toString()
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("name", text)
            startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY)
        }

        startServiceButton.setOnClickListener {
            startService()
        }

        startBroadcastButton.setOnClickListener {
            startService()
            startActivity(Intent(this, DynamicBroadcastReceiverActivity::class.java))
        }

        startContentProviderButton.setOnClickListener {
            startActivity(Intent(this, ContentProviderActivity::class.java))
        }
    }

    private fun startService() {
        val intent = Intent(this, MyService::class.java)
        intent.putExtra("name", "Zhanbolat")
        startService(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SECOND_ACTIVITY && resultCode == Activity.RESULT_OK) {
            resultView.text = data?.getStringExtra("key").orEmpty()
        }
    }
}
