package com.example.appcomponents

import android.content.Context
import android.net.Uri
import android.provider.MediaStore

object MediaUtils {

    private val projectionImages = arrayOf(
        MediaStore.Images.Media._ID,
        MediaStore.Images.Media.DATA,
        MediaStore.Images.Media.DATE_ADDED
    )

    fun getImages(context: Context): List<MediaItem> {
        val cursor = MediaStore.Images.Media.query(
            context.contentResolver,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projectionImages,
            null,
            null,
            MediaStore.Images.Media.DATE_ADDED + " DESC")

        val result = ArrayList<MediaItem>()

        cursor.use {
            if (it != null && it.moveToFirst()) {
                val imageIdColumn = it.getColumnIndex(MediaStore.Images.Media._ID)
                val dataColumn = it.getColumnIndex(MediaStore.Images.Media.DATA)
                do {
                    val imageId = it.getLong(imageIdColumn)
                    val path = it.getString(dataColumn)
                    result.add(
                        MediaItem(
                            itemId = imageId,
                            uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, imageId.toString()),
                            path = path
                        )
                    )
                } while (it.moveToNext())
            }
        }

        return result
    }
}