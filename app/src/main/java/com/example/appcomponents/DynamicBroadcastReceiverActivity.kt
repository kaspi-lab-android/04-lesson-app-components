package com.example.appcomponents

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_broadcast_receiver.*
import java.text.SimpleDateFormat
import java.util.*

class DynamicBroadcastReceiverActivity : AppCompatActivity() {

    var receiver1: BroadcastReceiver? = null
    var receiver2: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast_receiver)
        setCurrentDate()
        registerReceiver1()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver2()
    }

    private fun setCurrentDate() {
        dateView.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
    }

    private fun registerReceiver1() {
        receiver1 = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                when (intent?.action.orEmpty()) {
                    Intent.ACTION_POWER_CONNECTED -> resultView.text = "Power connected"
                    Intent.ACTION_POWER_DISCONNECTED -> resultView.text = "Power disconnected"
                    Intent.ACTION_TIME_TICK, Intent.ACTION_TIME_CHANGED -> setCurrentDate()
                }
            }
        }

        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED)
        filter.addAction(Intent.ACTION_POWER_CONNECTED)
        filter.addAction(Intent.ACTION_TIME_TICK)
        filter.addAction(Intent.ACTION_TIME_CHANGED)

        registerReceiver(receiver1, filter)
    }

    private fun registerReceiver2() {
        receiver2 = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                resultView.text = intent?.getStringExtra("key").orEmpty()
            }
        }

        val filter2 = IntentFilter(MyService.MY_CUSTOM_ACTION)
        registerReceiver(receiver2, filter2)
    }

    override fun onPause() {
        unregisterReceiver(receiver2)
        super.onPause()
    }

    override fun onDestroy() {
        unregisterReceiver(receiver1)
        super.onDestroy()
    }
}
