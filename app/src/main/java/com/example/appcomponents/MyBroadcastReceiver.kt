package com.example.appcomponents

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action.orEmpty()
        when (action) {
            Intent.ACTION_BOOT_COMPLETED -> {
                Log.d("MyBroadcastReceiver", "boot completed, update data from server")
            }
        }
    }
}