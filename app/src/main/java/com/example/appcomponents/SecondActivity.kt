package com.example.appcomponents

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val name = intent?.getStringExtra("name").orEmpty()
        resultView.text = name

        sendButton.setOnClickListener {
            val data = Intent()
            data.putExtra("key", name.reversed())
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }
}